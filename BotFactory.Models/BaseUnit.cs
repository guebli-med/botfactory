﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BotFactory.Common.Tools;
using System.Threading;

namespace BotFactory.Models
{
    public abstract class BaseUnit : ReportingUnit
    {
        public string Name { get; private set; }

        public double Speed { get; set; }


        public Coordinates CurrentPos { get; set; }


        public BaseUnit(string name, double speed = 1)
        {
            Name = name;

            CurrentPos = new Coordinates { X = 0, Y = 0 };
        }


        public async void Move(Coordinates destination)
        {
            Vector vector = Vector.FromCoordinates(CurrentPos, destination);

            int time = (int) (vector.Length() * 1000 / Speed); // time in milliseconds

            Thread.Sleep(time);
        }
    }
}
