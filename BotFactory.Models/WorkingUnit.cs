﻿using BotFactory.Common.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotFactory.Models
{
    public abstract class WorkingUnit : BaseUnit
    {
        public Coordinates ParkingPos { get; set; }

        public Coordinates WorkingPos { get; set; }

        public bool IsWorking { get; set; }

        public WorkingUnit(string name, double speed) : base(name, speed)
        {

        }

        public virtual async void WorkBegins()
        {

        }

        public virtual async void WorkEnds()
        {

        }
    }
}
