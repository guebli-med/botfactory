﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotFactory.Common.Tools
{
    public class Vector
    {
        public double X { get; set; }

        public double Y { get; set; }


        public static Vector FromCoordinates(Coordinates begin, Coordinates end)
        {
            return new Vector
            {
                X = (begin.X + end.X) / 2,
                Y = (begin.Y + end.Y) / 2,
            };
        }


        public double Length()
        {
            return Math.Sqrt(X * X + Y * Y);
        }
    }
}
